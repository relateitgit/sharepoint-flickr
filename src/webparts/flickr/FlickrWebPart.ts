import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'FlickrWebPartStrings';
import Flickr from './components/Flickr';
import { IFlickrProps } from './components/IFlickrProps';
import * as jQuery from "jquery";
export interface IFlickrWebPartProps {
  username: string;
  password: string;
  endpoint: string;
}

export default class FlickrWebPart extends BaseClientSideWebPart<IFlickrWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IFlickrProps > = React.createElement(
      Flickr,
      {
        username: this.properties.username,
        password: this.properties.password,
        endpoint: this.properties.endpoint
      }
    );

    ReactDom.render(element, this.domElement);
  }
  public onInit(): Promise<void> {
    return super.onInit().then(_ => {
        jQuery("#workbenchPageContent").prop("style", "max-width: none");
        jQuery(".SPCanvas-canvas").prop("style", "max-width: none");
        jQuery(".CanvasZone").prop("style", "max-width: none");
    });
}
  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  } 

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: "Flickr Settings",
              groupFields: [,
                PropertyPaneTextField('endpoint', {
                  label: "Endpoint"
                }),
                PropertyPaneTextField('username', {
                  label: "Username"
                }),
                PropertyPaneTextField('password', {
                  label: "Password"
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
