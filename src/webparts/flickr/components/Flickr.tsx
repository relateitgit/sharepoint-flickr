import * as React from 'react';
import styles from './Flickr.module.scss';
import { IFlickrProps } from './IFlickrProps';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import ReactGallery from 'reactive-blueimp-gallery';

import './styles.css';
export default class Flickr extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      currentItem: { 
        tagsarray: [] 
      },
      albums: [], 
      tags: [],
      selectedAlbum: "",  
      items: []
    };
    this.getAlbumImages = this.getAlbumImages.bind(this);
    this.getTagImages = this.getTagImages.bind(this);
    this.searchImages = this.searchImages.bind(this);
    this._onLoad = this._onLoad.bind(this);
    this._onSlide = this._onSlide.bind(this);
  }

  public componentDidMount() {
    this.getAlbums();
    this.getTags();
    this.getAllImages();
  }

  public getAllImages() {
    this.setState({
      loading: true
    });
    fetch(this.props.endpoint)
      .then(res => res.json())
      .then(res => {
        console.log(res);

        for (let i = 0; i < res.photos.photo.length; i++) {
          const element = res.photos.photo[i];
          element.source = element.url_o;
          element.tagsarray = element.tags.match(/\S+/g);
        }

        this.setState({
          items: res.photos.photo,
          loading: false
        });
      });

  }

  public getAlbumImages(album) {
    this.setState({
      loading: true
    });
    fetch(this.props.endpoint + "/albums/" + album)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        for (let i = 0; i < res.photoset.photo.length; i++) {
          const element = res.photoset.photo[i];
          element.source = element.url_o;
          element.tagsarray = element.tags.match(/\S+/g);
        }


        this.setState({
          items: res.photoset.photo,
          loading: false
        });
      });

  }

  public getTagImages(tag) {
    this.setState({
      loading: true
    });
    fetch(this.props.endpoint + "/tags/" + tag)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        for (let i = 0; i < res.photos.photo.length; i++) {
          const element = res.photos.photo[i];
          element.source = element.url_o;
          element.tagsarray = element.tags.match(/\S+/g);
        }
        this.setState({
          items: res.photos.photo,
          loading: false
        });
      });

  }

  public searchImages(event) {

    console.log(event);
    if(event.target.value){
    fetch(this.props.endpoint + "/search/" + event.target.value)
      .then(res => res.json())
      .then(res => {

        for (let i = 0; i < res.photos.photo.length; i++) {
          const element = res.photos.photo[i];
          element.source = element.url_o;
          element.tagsarray = element.tags.match(/\S+/g);
        }

        console.log(res);
        this.setState({
          items: res.photos.photo,

        });
      });
    } else {
      this.getAllImages();
    }
  }

  public getTags() {

    fetch(this.props.endpoint + "/tags")
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          tags: res.who.tags.tag,

        });
      });

  }

  public getAlbums() {

    fetch(this.props.endpoint + "/albums")
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          albums: res.photosets.photoset,

        });

      });

  }
  public _onLoad(gal) {
    console.log(gal);
    this.setState({ currentItem: this.state.items[gal.index] });
  }
  public _onSlide(gal) {
    console.log(gal);
    this.setState({ currentItem: this.state.items[gal] });
  }

  public render(): React.ReactElement<IFlickrProps> {
    const customOverlays = (
      <ReactGallery.Overlays>

        <div>
          <div className="left-tools">
            <h3>Download:</h3>
            <a target="_blank" href={this.state.currentItem.url_s} >Small ({this.state.currentItem.height_s} x {this.state.currentItem.width_s}) </a>
            <a target="_blank" href={this.state.currentItem.url_m} >Medium ({this.state.currentItem.height_m} x {this.state.currentItem.width_m}) </a>
            <a target="_blank" href={this.state.currentItem.url_l} >Large ({this.state.currentItem.height_l} x {this.state.currentItem.width_l}) </a>
            <a target="_blank" href={this.state.currentItem.url_o} >Original ({this.state.currentItem.height_o} x {this.state.currentItem.width_o}) </a>
            <h3>Tags:</h3> {this.state.currentItem.tagsarray.map((item, index) =>
              <span className="tag" key={index}>{item}</span>
            )}
          </div>
          <div className="right-tools">
            <h3>Info:</h3>
            <p><strong>Title:</strong> {this.state.currentItem.title}</p>
            <p><strong>Date:</strong> {this.state.currentItem.datetaken}</p>
            <p><strong>Format:</strong> {this.state.currentItem.originalformat}</p>
          </div>
        </div>

      </ReactGallery.Overlays>
    );

    return ( 
      <div className={styles.flickr}>
        <div className="ms-Grid" dir="ltr">
          <div className="ms-Grid-row">
          <div className="ms-Grid-col ms-sm12 ms-md12 ms-lg12">
          <input type="text" className="search-box" onKeyUp={this.searchImages} placeholder="Search" />
          <div className="info-box" ><p>All images are copyright protected and only to be used by Monjasa employees in a business context.</p></div>
          </div>
            <div className="ms-Grid-col ms-sm6 ms-md4 ms-lg2">
              <div className="box"> 
              
                <h2>Albums</h2>           
                {this.state.albums.map((item, index) => (
                  <p className="click-link" onClick={() => this.getAlbumImages(item.id)} key={index}>{item.title._content}</p>
                ))
                }
              </div>
              <div className="box">
                <h2>Tags</h2>
       
                {this.state.tags.map((item, index) => (
                  <span className="tag click-link" onClick={() => this.getTagImages(item._content)} key={index}>{item._content}</span>
                ))
                }
              </div>
            </div>
            <div className="ms-Grid-col ms-sm6 ms-md8 ms-lg10" id="mygallery">
              {this.state.loading &&
              <div className="loader">
                  <h4>Loading</h4>
                <Spinner size={SpinnerSize.large} />
              </div>
              }

              {!this.state.items.length && !this.state.loading &&
                <h2 className="text-center">No images found</h2>
              }
              { !this.state.loading &&
                <ReactGallery overlays={customOverlays} options={{ onopen: this._onLoad, onslide: this._onSlide, fullScreen: true }} withControls>
                  {this.state.items.map((item) => {

                    return <ReactGallery.Slide {...item} key={item.source} >

                    </ReactGallery.Slide>;

                  })}
                </ReactGallery>

              }
            </div>
          </div>
        </div>
      </div>
    );

  }

}
