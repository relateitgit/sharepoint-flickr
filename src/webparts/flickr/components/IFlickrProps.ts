export interface IFlickrProps {
  username: string;
  password: string;
  endpoint: string;
}
