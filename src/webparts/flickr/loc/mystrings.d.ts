declare interface IFlickrWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'FlickrWebPartStrings' {
  const strings: IFlickrWebPartStrings;
  export = strings;
}
